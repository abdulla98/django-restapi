from myapp.viewsets import MyappViewset, MunicipalityViewset
from rest_framework import routers

router = routers.DefaultRouter()
router.register('myapp', MyappViewset)
router.register('municipality', MunicipalityViewset)