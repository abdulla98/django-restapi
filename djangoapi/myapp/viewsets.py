from rest_framework import viewsets
from .models import Municipality, Myapp
from .serializers import MunicipalitySerializer, MyappSerializer
from rest_framework.permissions import IsAuthenticated
from django.shortcuts import render, redirect

class MyappViewset(viewsets.ModelViewSet):
    queryset = Myapp.objects.all()
    serializer_class = MyappSerializer
    permission_classes = [IsAuthenticated]

class MunicipalityViewset(viewsets.ModelViewSet):
    queryset = Municipality.objects.all()
    serializer_class = MunicipalitySerializer

def emp(request):  
    if request.method == "POST":  
        form = MunicipalitySerializer(request.POST)  
        if form.is_valid():  
            try:  
                form.save()  
                return redirect('/show')  
            except:  
                pass  
    else:  
        form = MunicipalitySerializer()  
    return render(request,'index.html',{'form':form})  
def show(request):  
    municipalities = Municipality.objects.all()  
    return render(request,"show.html",{'municipalities':municipalities})  
def edit(request, id):  
    municipality = Municipality.objects.get(id=id)  
    return render(request,'edit.html', {'municipality':municipality})  
def update(request, id):  
    municipality = Municipality.objects.get(id=8)  
    form = MunicipalitySerializer(request.POST, instance = municipality)  
    if form.is_valid():  
        form.save()  
        return redirect("/show")  
    return render(request, 'edit.html', {'municipality': municipality})  
def destroy(request, id):  
    municipality = Municipality.objects.get(id=id)  
    municipality.delete()  
    return redirect("/show")  