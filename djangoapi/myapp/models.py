from django.db import models
from django.contrib.gis.db import models
import jsonfield
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin

# Create your models here.
class Myapp(models.Model):
    title = models.CharField(max_length=70, blank=False, default='')
    description = models.CharField(max_length=200,blank=False, default='')
    published = models.BooleanField(default=False)

class Municipality(models.Model):
    type = models.CharField(max_length=70, blank=False, default='Feature')
    properties = jsonfield.JSONField()
    geometry = models.MultiPolygonField()