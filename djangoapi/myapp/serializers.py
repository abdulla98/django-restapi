
from rest_framework import serializers
from .models import Municipality, Myapp

class MyappSerializer(serializers.ModelSerializer):
    class Meta:
        model = Myapp
        fields = '__all__'

class MunicipalitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Municipality
        fields = '__all__'