import Circle from 'ol/geom/Circle';
import Feature from 'ol/Feature';
import GeoJSON from 'ol/format/GeoJSON';
import Map from 'ol/Map';
import View from 'ol/View';
import {Circle as CircleStyle, Fill, Stroke, Style} from 'ol/style';
import {OSM, Vector as VectorSource} from 'ol/source';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer';

// const geojsonObject = 
//       "http://127.0.0.1:8000/api/municipality/";
  
// // Defining async function
// async function getapi(url) {
    
//   // Storing response
//   const response = await fetch(url);

//   console.log(response);
  
//   // Storing data in form of JSON
//   var data = await response.json();
//   console.log(data);
// }
// // Calling that async function
// getapi(geojsonObject);

const image = new CircleStyle({
    radius: 5,
    fill: null,
    stroke: new Stroke({color: 'red', width: 1}),
  });
  
  const styles = {
    'Point': new Style({
      image: image,
    })
}

import data from './municipalities_nl.json';

console.log(data);

const vectorSource = new VectorSource({
    features: new GeoJSON().readFeatures(data),
  });

vectorSource.addFeature(new Feature(new Circle([5e6, 7e6], 1e6)));

const vectorLayer = new VectorLayer({
    source: vectorSource,
    style: styles.Point,
});

console.log(vectorLayer);

const map = new Map({
  layers: [
    new TileLayer({
      source: new OSM(),
    }),
    vectorLayer
  ],
  target: 'map',
  view: new View({
    center: [0, 0],
    zoom: 2,
  }),
});

document.getElementById('zoom-out').onclick = function () {
  const view = map.getView();
  const zoom = view.getZoom();
  view.setZoom(zoom - 1);
};

document.getElementById('zoom-in').onclick = function () {
  const view = map.getView();
  const zoom = view.getZoom();
  view.setZoom(zoom + 1);
};